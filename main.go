// You can edit this code!
// Click here and start typing.
package main

import "fmt"

type M map[string]any
type S []any
type A struct {
	A any
}

func (a *A) F(fields ...string) *A {
	for _, fld := range fields {
		if v, ok := a.A.(M); ok {
			if a.A, ok = v[fld]; ok {
				continue
			}
		}
		a.A = nil
		return a
	}
	return a
}
func (a *A) I(indexs ...int) *A {
	for _, idx := range indexs {
		if v, ok := a.A.(S); ok {
			if idx < len(v) {
				a.A = v[idx]
				continue
			}
		}
		a.A = nil
		return a
	}
	return a
}
func main() {
	fmt.Println("Hello, 世界")

	a := &A{A: M{"a": M{"b": M{"c": M{"d": M{"e": S{S{S{M{"f": 123}}}}}}}}}}
	a.F("a", "b", "c", "d", "e").I(0, 0, 0).F("f")
	if v, ok := a.A.(int); ok {
		println("val:", v)
	}
}
